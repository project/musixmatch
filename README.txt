* DESCRIPTION

This module allows embedding of lyrics from Musixmatch
(https://www.musixmatch.com) by rendering the URL as an iframe or a link.

* REQUIREMENT

The only requirement is the Field module.

* CONFIGURATION

1. Install and enable the module.
2. Add a Musixmatch field to any content type.
3. Configure the field settings (width, height & color theme) as desired.
4. Manage the display settings (iframe or link) as desired.
5. Enter a Musixmatch URL when adding new content of the content type.

* MAINTAINER
Jay Lee - https://www.drupal.org/user/1314572
