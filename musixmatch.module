<?php

/**
 * @file
 * Module file for Musixmatch.
 *
 * @author
 * Jay Lee - https://www.drupal.org/user/1314572
 */

/**
 * Implements hook_field_info().
 */
function musixmatch_field_info() {
  return array(
    'musixmatch' => array(
      'label' => t('Musixmatch'),
      'description' => t('This field stores a Musixmatch URL.'),
      'instance_settings' => array(
        'width'         => variable_get('musixmatch_default_width', '100%'),
        'height'        => variable_get('musixmatch_default_height', 380),
        'theme'         => 'light',
      ),
      'default_widget'    => 'musixmatch_url',
      'default_formatter' => 'musixmatch_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function musixmatch_field_widget_info() {
  return array(
    'musixmatch_url' => array(
      'label'       => t('Musixmatch URL'),
      'field types' => array('musixmatch'),
      'behaviors'   => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value'   => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function musixmatch_field_formatter_info() {
  return array(
    'musixmatch_iframe' => array(
      'label'       => t('Iframe'),
      'field types' => array('musixmatch'),
    ),
    'musixmatch_link' => array(
      'label'       => t('Link'),
      'field types' => array('musixmatch'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function musixmatch_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $base = $element;

  if ($instance['widget']['type'] == 'musixmatch_url') {
    $element['url'] = array(
      '#type'           => 'textfield',
      '#default_value'  => isset($items[$delta]['url']) ? $items[$delta]['url'] : NULL,
    ) + $base;
  }

  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function musixmatch_field_is_empty($item, $field) {
  if ($field['type'] == 'musixmatch') {
    if (empty($item['url'])) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implements hook_field_formatter_view().
 */
function musixmatch_field_formatter_view($obj_type, $object, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $instance['settings']['musixmatch'];
  $theme = ($settings['theme']) ? 'true' : 'false';

  switch ($display['type']) {

    case 'musixmatch_iframe':
      foreach ($items as $delta => $item) {
        $url = $item['url'];

        if ($settings['theme'] == '1') {
          $theme = 'dark';
        }
        else {
          $theme = 'light';
        }

        $iframe = '<iframe src="' . check_url($url) . '/embed?theme=' . $theme . '" style="border:none;background:transparent;" width="' . $settings['width'] . '" height="' . $settings['height'] . '" border=0> </iframe>';

        $output = html_entity_decode($iframe);

        $element[$delta] = array('#markup' => $output);
      }
      break;

    case 'musixmatch_link':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => l($item['url'], $item['url']));
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_validate().
 */
function musixmatch_field_validate($obj_type, $object, $field, $instance, $langcode, &$items, &$errors) {
  if ($field['type'] == 'musixmatch') {
    foreach ($items as $delta => $item) {
      if (!empty($item['url']) && !preg_match('@^https://www\.musixmatch\.com/([^"\&]+)@i', $item['url'], $matches)) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error'   => 'musixmatch_invalid_url',
          'message' => t('%url is not a valid Musixmatch URL.', array('%url' => $item['url'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_widget_error().
 */
function musixmatch_field_widget_error($element, $error) {
  switch ($error['error']) {
    case 'musixmatch_invalid_url':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_field_instance_settings_form().
 */
function musixmatch_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  if ($field['type'] == 'musixmatch') {
    $form['musixmatch'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Musixmatch settings'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
    );
    $form['musixmatch']['width'] = array(
      '#type'             => 'textfield',
      '#title'            => t('Width'),
      '#size'             => 4,
      '#default_value'    => empty($settings['musixmatch']['width']) ? variable_get('musixmatch_default_width', '100%') : $settings['musixmatch']['width'],
      '#description'      => t('Width (Default: @width)', array('@width' => variable_get('musixmatch_default_width', '100%'))),
      '#required'         => TRUE,
    );
    $form['musixmatch']['height'] = array(
      '#type'             => 'textfield',
      '#title'            => t('Height'),
      '#size'             => 4,
      '#default_value'    => empty($settings['musixmatch']['height']) ? variable_get('musixmatch_default_height', 380) : $settings['musixmatch']['height'],
      '#description'      => t('Height (Default: @height)', array('@height' => variable_get('musixmatch_default_height', 380))),
      '#required'         => TRUE,
    );
    $form['musixmatch']['theme'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Dark color theme'),
      '#default_value'  => empty($settings['musixmatch']['theme']) ? FALSE : $settings['musixmatch']['theme'],
      '#description'    => t('Dark color theme (Default: Light color theme)'),
    );
  }

  return $form;
}
